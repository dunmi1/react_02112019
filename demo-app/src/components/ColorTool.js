import React, { useState } from 'react';

import { ToolHeader } from './ToolHeader';
import { ColorList } from './ColorList';
import { ColorForm } from './ColorForm';

export const ColorTool = (props) => {

  const [ colors, setColors ] = useState(props.colors.concat());

  const [ test, setTest ] = useState('');

  const addColor = (color) => {
    setColors(colors.concat(color));
  };

  const deleteColor = (color) => {
    setColors(colors.filter(c => c !== color));
  };

  return <>
    <ToolHeader headerText="Color Tool" />
    <ColorList colors={colors} onDeleteColor={deleteColor} />
    <ColorForm buttonText="Add Color" onSubmitColor={addColor} />
    <input type="text" value={test} onChange={e => setTest(e.target.value)} />
    <span>{test}</span>
  </>;


};