Exercise 10

1. Create a new component named CarEditRow. The component will display a table row of car data but the make, model, year, color and price columns will be input fields. Initially, populate the input fields with the car data passed via props. The id should not be an input field.

2. Add a new button to CarViewRow labeled 'Edit'. When the edit button is clicked, the CarViewRow for that car will change to CarEditRow. Only one car is editable at a time.

3. Do NOT add a save or cancel button, and do NOT implement save and cancel functionality. Only implement the 'Edit' button.

4. Ensure it works.