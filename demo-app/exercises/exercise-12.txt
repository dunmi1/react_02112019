Exercise 12

1. Add a Save and Cancel button to the EditCarRow component.

2. When the Save button is clicked, the changes are saved to the cars array in CarTool and the EditCarRow changes to a ViewCarRow.

3. When the Cancel button is clicked, the changes are discarded and the EditCarRow changes to a ViewCarRow.

4. If a car is being edited, and another car is deleted (the Delete Car button is clicked), then the changes in the EditCarRow are discarded and the EditCarRow changes to ViewCarRow.

5. If a car is being edited, and a new car is added (the Add Car button is clicked), then the changes in the EditCarRow are discarded and the EditCarRow changes to ViewCarRow.

6. Ensure it all works.